# R1
## R2
### R3
#### R4
##### R5
###### R6




#El tiempo España

La predicción del tiempo para hoy en España

Previsión para España - Disminuyen las temperaturas. Hoy en el Norte de España cielos poco nubosos y temperaturas de 16ºC y 21ºC.

En el Noroeste del País cielos cubiertos y temperaturas de 17ºC y 19ºC.

Mientras que el tiempo en el Noreste de España, cielos despejados, ambiente soleado con temperaturas que oscilarán entre 20ºC y 24ºC. Seguidamente en el Centro de la Península cielos poco nubosos y temperaturas de 18ºC y 23ºC.

Por otro lado, la previsión meteorológica en el Este del País, cielos poco nubosos con temperaturas que oscilarán entre 23ºC y 28ºC. En la zona Sur cielos poco nubosos con temperaturas que oscilarán entre 21ºC y 29ºC.

Por lo que respecta a las Islas, la previsión del tiempo en Baleares, cielos poco nubosos con temperaturas que oscilarán entre 22ºC y 25ºC mientras que en Canarias habrá nubes con temperaturas altas rondando los 25ºC.
